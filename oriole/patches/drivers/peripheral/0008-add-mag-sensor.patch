From 251abd35cbf7c3030d1a6efb080e9473b4d52bde Mon Sep 17 00:00:00 2001
From: liuchao <380263137@qq.com>
Date: Fri, 8 Nov 2024 09:38:13 +0800
Subject: [PATCH] add mag sensor

Change-Id: I2d988ac436ada0199bbd7770ea369ee02d8b3429
Signed-off-by: liuchao <380263137@qq.com>
---
 sensor/chipset/magnetic/magnetic_mmc5603nj.c | 198 +++++++++++++++++++
 sensor/chipset/magnetic/magnetic_mmc5603nj.h |  83 ++++++++
 2 files changed, 281 insertions(+)
 create mode 100644 sensor/chipset/magnetic/magnetic_mmc5603nj.c
 create mode 100644 sensor/chipset/magnetic/magnetic_mmc5603nj.h

diff --git a/sensor/chipset/magnetic/magnetic_mmc5603nj.c b/sensor/chipset/magnetic/magnetic_mmc5603nj.c
new file mode 100644
index 000000000..84d639b65
--- /dev/null
+++ b/sensor/chipset/magnetic/magnetic_mmc5603nj.c
@@ -0,0 +1,198 @@
+/*
+ * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
+ *
+ * HDF is dual licensed: you can use it either under the terms of
+ * the GPL, or the BSD license, at your option.
+ * See the LICENSE file in the root of this repository for complete details.
+ */
+
+#include "magnetic_mmc5603nj.h"
+#include <securec.h>
+#include "osal_mem.h"
+#include "osal_time.h"
+#include "sensor_config_controller.h"
+#include "sensor_device_manager.h"
+#include "sensor_magnetic_driver.h"
+
+#define HDF_LOG_TAG    khdf_sensor_magnetic_driver
+
+static struct Mmc5603njDrvData *g_mmc5603njDrvData = NULL;
+
+/* IO config for int-pin and I2C-pin */
+#define SENSOR_I2C6_DATA_REG_ADDR 0x114f004c
+#define SENSOR_I2C6_CLK_REG_ADDR  0x114f0048
+#define SENSOR_I2C_REG_CFG        0x403
+
+static int32_t ReadMmc5603njRawData(struct SensorCfgData *data, struct MagneticData *rawData, uint64_t *timestamp)
+{
+    int i, ret;
+    uint8_t reg[6];
+    OsalTimespec time;
+
+    (void)memset_s(&time, sizeof(time), 0, sizeof(time));
+    (void)memset_s(reg, sizeof(reg), 0, sizeof(reg));
+
+    CHECK_NULL_PTR_RETURN_VALUE(data, HDF_ERR_INVALID_PARAM);
+
+    if (OsalGetTime(&time) != HDF_SUCCESS) {
+        HDF_LOGE("%s: Get time failed", __func__);
+        return HDF_FAILURE;
+    }
+    *timestamp = time.sec * SENSOR_SECOND_CONVERT_NANOSECOND + time.usec * SENSOR_CONVERT_UNIT; /* unit nanosecond */
+
+    for (i = 0; i < 6; i++) {
+        ret = ReadSensor(&data->busCfg, i, &reg[i], sizeof(uint8_t));
+        CHECK_PARSER_RESULT_RETURN_VALUE(ret, "read data");
+    }
+
+    rawData->x = (int16_t)((uint16_t)(reg[0] << 8 | reg[1]) - 32768);
+    rawData->y = (int16_t)((uint16_t)(reg[2] << 8 | reg[3]) - 32768);
+    rawData->z = (int16_t)((uint16_t)(reg[4] << 8 | reg[5]) - 32768);
+
+    return HDF_SUCCESS;
+}
+
+static int32_t ReadMmc5603njData(struct SensorCfgData *data)
+{
+    struct MagneticData rawData = { 0, 0, 0 };
+    int32_t tmp[MAGNETIC_AXIS_NUM];
+    struct SensorReportEvent event;
+
+    (void)memset_s(&event, sizeof(event), 0, sizeof(event));
+    (void)memset_s(tmp, sizeof(tmp), 0, sizeof(tmp));
+
+    CHECK_NULL_PTR_RETURN_VALUE(data, HDF_ERR_INVALID_PARAM);
+
+    int32_t ret = ReadMmc5603njRawData(data, &rawData, &event.timestamp);
+    if (ret != HDF_SUCCESS) {
+        HDF_LOGE("%s: MMC5603NJ read raw data failed", __func__);
+        return HDF_FAILURE;
+    }
+
+    event.sensorId = SENSOR_TAG_MAGNETIC_FIELD;
+    event.option = 0;
+    event.mode = SENSOR_WORK_MODE_REALTIME;
+
+    tmp[MAGNETIC_X_AXIS] = rawData.x >> 10;
+    tmp[MAGNETIC_Y_AXIS] = rawData.y >> 10;
+    tmp[MAGNETIC_Z_AXIS] = rawData.z >> 10;
+
+    ret = SensorRawDataToRemapData(data->direction, tmp, sizeof(tmp) / sizeof(tmp[0]));
+    if (ret != HDF_SUCCESS) {
+        HDF_LOGE("%s: MMC5603NJ convert raw data failed", __func__);
+        return HDF_FAILURE;
+    }
+
+    event.dataLen = sizeof(tmp);
+    event.data = (uint8_t *)&tmp;
+    ret = ReportSensorEvent(&event);
+    if (ret != HDF_SUCCESS) {
+        HDF_LOGE("%s: MMC5603NJ report data failed", __func__);
+    }
+
+    return ret;
+}
+
+static int32_t InitMmc5603nj(struct SensorCfgData *data)
+{
+    int32_t ret;
+
+    CHECK_NULL_PTR_RETURN_VALUE(data, HDF_ERR_INVALID_PARAM);
+
+    ret = SetSensorRegCfgArray(&data->busCfg, data->regCfgGroup[SENSOR_INIT_GROUP]);
+    if (ret != HDF_SUCCESS) {
+        HDF_LOGE("%s: Mmc5603nj sensor init config failed", __func__);
+        return HDF_FAILURE;
+    }
+
+    return HDF_SUCCESS;
+}
+
+static int32_t DispatchMmc5603nj(struct HdfDeviceIoClient *client,
+    int cmd, struct HdfSBuf *data, struct HdfSBuf *reply)
+{
+    (void)client;
+    (void)cmd;
+    (void)data;
+    (void)reply;
+
+    return HDF_SUCCESS;
+}
+
+static int32_t Mmc5603njBindDriver(struct HdfDeviceObject *device)
+{
+    CHECK_NULL_PTR_RETURN_VALUE(device, HDF_ERR_INVALID_PARAM);
+
+    struct Mmc5603njDrvData *drvData = (struct Mmc5603njDrvData *)OsalMemCalloc(sizeof(*drvData));
+    if (drvData == NULL) {
+        HDF_LOGE("%s: Malloc Mmc5603nj drv data fail", __func__);
+        return HDF_ERR_MALLOC_FAIL;
+    }
+
+    drvData->ioService.Dispatch = DispatchMmc5603nj;
+    drvData->device = device;
+    device->service = &drvData->ioService;
+    g_mmc5603njDrvData = drvData;
+
+    return HDF_SUCCESS;
+}
+
+static int32_t Mmc5603njInitDriver(struct HdfDeviceObject *device)
+{
+    int32_t ret;
+    struct MagneticOpsCall ops;
+
+    CHECK_NULL_PTR_RETURN_VALUE(device, HDF_ERR_INVALID_PARAM);
+    struct Mmc5603njDrvData *drvData = (struct Mmc5603njDrvData *)device->service;
+    CHECK_NULL_PTR_RETURN_VALUE(drvData, HDF_ERR_INVALID_PARAM);
+
+    if (device->property == NULL) {
+        HDF_LOGE("%s: liuchao device->property null", __func__);
+    }
+
+    drvData->sensorCfg = MagneticCreateCfgData(device->property);
+    if (drvData->sensorCfg == NULL || drvData->sensorCfg->root == NULL) {
+        HDF_LOGD("%s: Creating magneticcfg failed because detection failed", __func__);
+        return HDF_ERR_NOT_SUPPORT;
+    }
+
+    ops.Init = NULL;
+    ops.ReadData = ReadMmc5603njData;
+    ret = MagneticRegisterChipOps(&ops);
+    if (ret != HDF_SUCCESS) {
+        HDF_LOGE("%s: Register mmc5603nj magnetic failed", __func__);
+        return HDF_FAILURE;
+    }
+
+    ret = InitMmc5603nj(drvData->sensorCfg);
+    if (ret != HDF_SUCCESS) {
+        HDF_LOGE("%s: Init mmc5603nj magnetic failed", __func__);
+        return HDF_FAILURE;
+    }
+
+    return HDF_SUCCESS;
+}
+
+static void Mmc5603njReleaseDriver(struct HdfDeviceObject *device)
+{
+    CHECK_NULL_PTR_RETURN(device);
+
+    struct Mmc5603njDrvData *drvData = (struct Mmc5603njDrvData *)device->service;
+    CHECK_NULL_PTR_RETURN(drvData);
+
+    if (drvData->sensorCfg != NULL) {
+        MagneticReleaseCfgData(drvData->sensorCfg);
+        drvData->sensorCfg = NULL;
+    }
+    OsalMemFree(drvData);
+}
+
+struct HdfDriverEntry g_magneticMmc5603njDevEntry = {
+    .moduleVersion = 1,
+    .moduleName = "HDF_SENSOR_MAGNETIC_MMC5603NJ",
+    .Bind = Mmc5603njBindDriver,
+    .Init = Mmc5603njInitDriver,
+    .Release = Mmc5603njReleaseDriver,
+};
+
+HDF_INIT(g_magneticMmc5603njDevEntry);
\ No newline at end of file
diff --git a/sensor/chipset/magnetic/magnetic_mmc5603nj.h b/sensor/chipset/magnetic/magnetic_mmc5603nj.h
new file mode 100644
index 000000000..ec74fad44
--- /dev/null
+++ b/sensor/chipset/magnetic/magnetic_mmc5603nj.h
@@ -0,0 +1,83 @@
+/*
+ * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
+ *
+ * HDF is dual licensed: you can use it either under the terms of
+ * the GPL, or the BSD license, at your option.
+ * See the LICENSE file in the root of this repository for complete details.
+ */
+
+#ifndef MAGNETIC_MMC5603NJ_H
+#define MAGNETIC_MMC5603NJ_H
+
+#include "sensor_config_parser.h"
+#include "sensor_magnetic_driver.h"
+
+#define MMC5603NJ_MAGNETIC_GIN                     1000
+
+/* MAGNETIC SET RATE AND MODE ADDR */
+#define MMC5603NJ_CRA_REG_ADDR                     0X00
+#define MMC5603NJ_CRB_REG_ADDR                     0X01
+#define MMC5603NJ_MR_REG_ADDR                      0X02
+
+/* MAGNETIC DATA REGISTERS ADDR */
+#define MMC5603NJ_MAGNETIC_X_MSB_ADDR              0X03
+#define MMC5603NJ_MAGNETIC_X_LSB_ADDR              0X04
+#define MMC5603NJ_MAGNETIC_Y_MSB_ADDR              0X05
+#define MMC5603NJ_MAGNETIC_Y_LSB_ADDR              0X06
+#define MMC5603NJ_MAGNETIC_Z_MSB_ADDR              0X07
+#define MMC5603NJ_MAGNETIC_Z_LSB_ADDR              0X08
+#define MMC5603NJ_STATUS_ADDR                      0X18
+
+/* MAGNETIC DATA RATE CONFIG HZ */
+#define MMC5603NJ_DATA_RATE_0                      0X00
+#define MMC5603NJ_DATA_RATE_1                      0X04
+#define MMC5603NJ_DATA_RATE_2                      0X08
+#define MMC5603NJ_DATA_RATE_3                      0X0C
+#define MMC5603NJ_DATA_RATE_4                      0X10
+#define MMC5603NJ_DATA_RATE_5                      0X14
+#define MMC5603NJ_DATA_RATE_6                      0X18
+#define MMC5603NJ_DATA_RATE_7                      0X1C
+
+/* MAGNETIC GAIN CONFIG GAUSS */
+#define MMC5603NJ_GAIN_RATE_0                      0X20
+#define MMC5603NJ_GAIN_RATE_1                      0X40
+#define MMC5603NJ_GAIN_RATE_2                      0X60
+#define MMC5603NJ_GAIN_RATE_3                      0X80
+#define MMC5603NJ_GAIN_RATE_4                      0XA0
+#define MMC5603NJ_GAIN_RATE_5                      0XC0
+#define MMC5603NJ_GAIN_RATE_6                      0XE0
+
+/* MAGNETIC GAIN SENSITIVITY RANGE */
+#define MMC5603NJDLHC_SENSITIVITY_XY13GA           1100
+#define MMC5603NJDLHC_SENSITIVITY_XY19GA           855
+#define MMC5603NJDLHC_SENSITIVITY_XY25GA           670
+#define MMC5603NJDLHC_SENSITIVITY_XY40GA           450
+#define MMC5603NJDLHC_SENSITIVITY_XY47GA           400
+#define MMC5603NJDLHC_SENSITIVITY_XY56GA           330
+#define MMC5603NJDLHC_SENSITIVITY_XY81GA           230
+#define MMC5603NJDLHC_SENSITIVITY_Z13GA            980
+#define MMC5603NJDLHC_SENSITIVITY_Z19GA            760
+#define MMC5603NJDLHC_SENSITIVITY_Z25GA            600
+#define MMC5603NJDLHC_SENSITIVITY_Z40GA            400
+#define MMC5603NJDLHC_SENSITIVITY_Z47GA            355
+#define MMC5603NJDLHC_SENSITIVITY_Z56GA            295
+#define MMC5603NJDLHC_SENSITIVITY_Z81GA            205
+
+/* MAGNETIC MODE CONFIG */
+#define MMC5603NJ_OPERATING_MODE_1                 0X00
+#define MMC5603NJ_OPERATING_MODE_2                 0X01
+#define MMC5603NJ_OPERATING_MODE_3                 0X02
+#define MMC5603NJ_OPERATING_MODE_4                 0X03
+
+/* MAGNETIC DATA READY */
+#define MMC5603NJ_DATA_READY_MASK                  0x01
+
+int32_t DetectMagneticMmc5603njChip(struct SensorCfgData *data);
+
+struct Mmc5603njDrvData {
+    struct IDeviceIoService ioService;
+    struct HdfDeviceObject *device;
+    struct SensorCfgData *sensorCfg;
+};
+
+#endif /* MAGNETIC_MMC5603NJ_H */
\ No newline at end of file
-- 
2.25.1

